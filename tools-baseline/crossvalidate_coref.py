#!/usr/bin/env python3
# -*- coding: utf8 -*-

# Marmara Turkish Coreference Corpus Baseline: Coreference Crossvalidation.
# Copyright (C) 2015-2017 Peter Schüller <schueller.p@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys, re, os, random, itertools, traceback, time
import subprocess
import argparse
import pickle
import sklearn.svm
import sklearn.linear_model
import sklearn.feature_extraction
from sklearn import metrics

import knowlpcoref

# take only first LIMIT tokens (None for no limit) - this is for shorter runs during development
LIMIT=None

XML_TO_CONLL_CONVERTER='python3 xml-to-conll.py'
CONLL_SCORER='../reference-coreference-scorers/scorer.pl'

message = knowlpcoref.message
warn = knowlpcoref.warn
warnOnce = knowlpcoref.warnOnce
debug = knowlpcoref.debug

def learn(train_x, train_y):
  global config
  vec = sklearn.feature_extraction.DictVectorizer()
  train_x = list(train_x)
  train_y = list(train_y)
  debug('transforming {}/{} examples/labels'.format(len(train_x), len(train_y)))
  X = vec.fit_transform(train_x)
  Y = train_y

  debug("learning using method '{}'".format(config['method']))
  #model = svm.SVC(kernel='linear', C=30, gamma=0.1, cache_size=2000) 
  # instead of SVC kernel=linear we can use LinearSVC

  if config['method'] == 'default':
    model = sklearn.svm.LinearSVC(C=1.0, verbose=1, class_weight='balanced', max_iter=1000)
  else:
    model = eval(config['method'])

  # F1 = 0.6
  #model = sklearn.linear_model.Perceptron(penalty='l2', alpha=0.005, n_iter=50, class_weight='balanced')

  # there is various option associated with it, like changing kernel, gamma and C value. Will discuss more # about it in next section.Train the model using the training sets and check score
  model.fit(X, Y)

  # score of training data (shows if our features are enough to classify the training data)
  modeltrainf1 = model.score(X, Y)

  return vec, model, modeltrainf1


def printScores(docname, modelf1, Ytest, Ypred, scores):
  hdr = 'doc_bound MF1 F1 P R MTH m.f1 muc.f1 bcub.f1 ceafe.f1 ceafm.f1 blanc.f1'.split(' ')
  out = [docname]
  out += [modelf1*100.0]

  if Ytest is not None and not config['regression']:
    f1 = metrics.f1_score(Ytest, Ypred)
    p = metrics.precision_score(Ytest, Ypred)
    r = metrics.recall_score(Ytest, Ypred)
    m = metrics.matthews_corrcoef(Ytest, Ypred)
    #print("{} F1 {:.4f} P {:.4f} R {:.4f} M {:.4f}".format(foldname, f1, p, r, m))
    #print(metrics.classification_report(Ytest, Tpred, digits=3))
    #message("MF1;{};F1;{};P;{};R;{};M;{}".format(foldname, f1, p, r, m))
    out += [f1*100.0, p*100.0, r*100.0, m*100.0]
  else:
    out += ['none']*4

  METRICS = ['muc', 'bcub', 'ceafe', 'ceafm', 'blanc']
  #MSCORERSCORES = [ '{}.m.f1'.format(m) for m in METRICS ]
  MSCORERSCORES = [ 'muc.m.f1' ]
  CSCORERSCORES = [ '{}.c.f1'.format(m) for m in METRICS ] # show all
  SHOWNSCORES=MSCORERSCORES+CSCORERSCORES
  for s in SHOWNSCORES:
    out += [scores[s]]
  # we have some numpy and other datatypes in here which cannot be joined to a string
  message('\n'+';'.join(hdr)+'\n'+';'.join(map(str, out)))

def scoreFromFiles(docname, htmfile, xmlgold, xmlpred):
  debug('starting scoreFromFiles '+docname)
  # converted predicted coref to conll
  predconll = os.path.join(OUTDIR, '{}.goldmentions.predcoref.conll'.format(docname))
  cmd = "{} {} {} {} {}".format(XML_TO_CONLL_CONVERTER, htmfile, xmlpred, docname, predconll)
  debug('running '+cmd)
  subprocess.check_call(cmd, shell=True, stdout=sys.stdout, stderr=sys.stderr)
  # convert gold coref to conll
  goldconll = os.path.join(OUTDIR, '{}.goldmentions.goldcoref.conll'.format(docname))
  cmd = "{} {} {} {} {}".format(XML_TO_CONLL_CONVERTER, htmfile, xmlgold, docname, goldconll)
  debug('running '+cmd)
  # run scorer
  subprocess.check_call(cmd, shell=True, stdout=sys.stdout, stderr=sys.stderr)
  cmd = "{} all {} {}".format(CONLL_SCORER, goldconll, predconll)
  debug('running '+cmd)
  out = subprocess.check_output(cmd, shell=True, stderr=sys.stderr)
  # interpret and return scorer output
  return knowlpcoref.parseScorerLogContent(out)

def runOneFold(testdocname, docs, docs_feat, docs_files):
  debug("Fold: test={}".format(testdocname))
  # train on all others
  traindocnames = [n for n in docs.keys() if n != testdocname]
  debug("traindocnames {}".format(repr(traindocnames)))

  # learning
  train_x = list(itertools.chain(*[ docs_feat[tdn]['features_gold'] for tdn in traindocnames ]))
  train_y = list(itertools.chain(*[ docs_feat[tdn]['mmlabel_gold'] for tdn in traindocnames ]))

  debug("now learning from {} examples".format(len(train_x)))
  vec, model, modeltrainf1 = learn(train_x, train_y)
  modelfile = os.path.join(OUTDIR, '{}.model.pk'.format(testdocname))
  pkg = { 'vec':vec, 'model':model }
  debug("writing model (F1={}) to {}".format(modeltrainf1, modelfile))
  with open(modelfile, 'wb') as of:
    pickle.dump(pkg, of)

  # prediction
  use, which = 'gold', 'mentions'
  Ygold = docs_feat[testdocname]['mmlabel_gold']
  if config['predictedmentions']:
    use, which = 'pred', 'predmentions'
    Ygold = None
    # deactivate scoring Ypred (below) because we have no gold data for this
    # TODO implement scoring Ypred on gold training examples (less important than scoring test data)
    # we still use the prediction to create XML and CONLL and to score CONLL (which is the main point anyways)

  dev_x = docs_feat[testdocname]['features_'+use]

  Xtest = vec.transform(dev_x)
  Ypred = model.predict(Xtest)
  debug("Ypred ranges from {} to {} with average {}".format(min(Ypred), max(Ypred), sum(Ypred)/len(dev_x)))
  #f = sorted(Ypred)[int(len(dev_x)/3):-int(len(dev_x)/3)]
  #debug(repr(Ypred))
  #debug(repr(f))
  #debug("Ypred sorted only middle ranges from {} to {} with average {}".format(min(f), max(f), sum(f)*3.0/len(dev_x)))

  whichmentions = docs[testdocname]['mentions']
  if config['predictedmentions']:
    whichmentions = docs[testdocname]['predmentions']
  if config['regression']:
    # multiple predictions
    for regbound in config['regbounds']:
      chains = knowlpcoref.build_chains_regression(docs_feat[testdocname]['mmcand_'+use], Ypred, whichmentions, regbound)
      xmlname = '{}.{}mentions.predcoref.{}.xml'.format(testdocname, use, regbound)
      scores = writeXMLGetScores(xmlname, docs, docs_files, testdocname, docs[testdocname][which], chains)
      printScores(testdocname+'_'+str(regbound), modeltrainf1, Ygold, Ypred, scores)
  else:
    # one prediction, placeholder for bound
    chains = knowlpcoref.build_chains_classification(docs_feat[testdocname]['mmcand_'+use], Ypred, whichmentions)
    xmlname = '{}.{}mentions.predcoref.xml'.format(testdocname, use)
    scores = writeXMLGetScores(xmlname, docs, docs_files, testdocname, docs[testdocname][which], chains)
    printScores(testdocname+'_none', modeltrainf1, Ygold, Ypred, scores)

def writeXMLGetScores(xmlname, docs, docs_files, testdocname, mentions, chains):
  xmlfile = os.path.join(OUTDIR, xmlname)
  debug("writing predicted coref to {}".format(xmlfile))
  with open(xmlfile, 'w') as of:
    of.write(knowlpcoref.createCorefXML(mentions, chains))
  scores = scoreFromFiles(testdocname, docs_files[testdocname]['htm'], docs_files[testdocname]['gold'], xmlfile)
  return scores

def enrichDocsWithPredictedMentions(docs, docs_files):
  for docname, docfiles in docs_files.items():
    docfile = docfiles['htm']
    predxmlfile = os.path.join(OUTDIR, '{}.predmentions.conll'.format(docname))
    docfiles['predmention'] = predxmlfile
    cmd = "./predictmentions.py --inp={} --out={}".format(docfile, predxmlfile)
    debug('running '+cmd)
    subprocess.check_call(cmd, shell=True)
    m_ch = knowlpcoref.getMentionsChainsFromXML(open(predxmlfile, "r").read())
    if LIMIT:
      # discard all but first LIMIT/2 mentions
      m_ch['mentions'] = dict(list(m_ch['mentions'].items())[:LIMIT])
    # store predicted mentions in doc but give them new IDs (they are not in any chains yet anyways)
    d = docs[docname]
    maxmid = int(d['maxmid'])
    d['predmentions'] = dict([ (int(m[0])+maxmid, m[1]) for m in m_ch['mentions'].items() ])
    #debug("predicted mentions after ID transformation {}".format(repr(d['predmentions'])))
    # enrich them (or we will not be able to build features)
    knowlpcoref.enrichMentions(d['tokens'], d['token_by_s_ix'], d['predmentions'])
    # find those mentions that are predicted but not in gold
    canonicalmentions = set([ (m['token_from'], m['token_to']) for m in d['mentions'].values() ])
    # (we will use them for training the coreference mention-mention classifier)
    potentialextrametionsfortraining = [ m for m in d['predmentions'].items()
      if (m[1]['token_from'], m[1]['token_to']) not in canonicalmentions]
    #d['extramentions'] = dict(potentialextrametionsfortraining)
    # the potential ones are too much
    #nitems = int(maxmid/2) # sample 50% of existing gold mentions from these
    nitems = int(maxmid) # sample 100% of existing gold mentions from these
    d['extramentions'] = dict(random.sample(potentialextrametionsfortraining, min(nitems, len(potentialextrametionsfortraining))))
    #debug("extramentions {}".format(repr(d['extramentions'])))

def main():
  try:
    interpretArguments()
    debug('config='+repr(config))

    # read docs
    docs = {}
    docs_files = {}
    for td in config['docs']:
      debug("loading doc {}".format(td['doc']))
      # extramentions = predicted mentions that are not gold mentions that are used for training to predict coreference on predicted mentions
      # these are not all predicted mentions (those would be too many)
      docs_files[td['doc']] = {'htm':td['docfile'], 'gold':td['crfile'], 'extramentions':{}}
      d = knowlpcoref.importDocPlusCoref(td['docfile'], td['crfile'], LIMIT)
      knowlpcoref.enrichMentions(d['tokens'], d['token_by_s_ix'], d['mentions'])
      d['maxmid'] = max(d['mentions'].keys())
      docs[td['doc']] = d
    if config['predictedmentions']:
      debug("predicting mentions")
      enrichDocsWithPredictedMentions(docs, docs_files)

    # create candidates, gold labels, features for docs (gold)
    debug("building features")
    docs_feat = {}
    # go over docs in order specified at commandline! (dict has order according to memory region)
    for td in config['docs']:
      docname = td['doc']
      doc = docs[docname]
      mentionbase = doc['mentions']
      # if we predict mentions we also add them to the gold (for better training)
      extra = ''
      if config['predictedmentions']:
        mentionbase.update(doc['extramentions'])
        extra = ' using {} extra predicted mentions'.format(len(doc['extramentions']))
      # create candidate tuples and features for learning from this document
      mmcandidates_g, mml_g = knowlpcoref.create_mention_mention_tuples_gold(doc['tokens'], mentionbase, doc['chains'])
      mmf_g = knowlpcoref.create_mention_mention_features(doc['tokens'], mentionbase, mmcandidates_g)
      debug("for doc {} created {} features and {} gold labels{}".format(docname, len(mmf_g), len(mml_g), extra))
      docs_feat[docname] = {'mmcand_gold':mmcandidates_g, 'mmlabel_gold': mml_g, 'features_gold': mmf_g}
      if config['predictedmentions']:
        # create candidate tuples and features for testing on this document (only on predicted mentions)
        mmcandidates_p = knowlpcoref.create_mention_mention_tuples(doc['tokens'], doc['predmentions'])
        mmf_p = knowlpcoref.create_mention_mention_features(doc['tokens'], doc['predmentions'], mmcandidates_p)
        debug("for doc {} created {} features from predicted mentions".format(docname, len(mmf_p)))
        docs_feat[docname].update({ 'mmcand_pred':mmcandidates_p, 'features_pred': mmf_p })

    # free memory (fasttext process require a lot of memory)
    knowlpcoref.del_ftfc()

    # crossvalidation
    for testdocname in docs.keys():
      runOneFold(testdocname, docs, docs_feat, docs_files)
  except:
    warn(traceback.format_exc())

def interpretArguments():
  global config, OUTDIR
  parser = argparse.ArgumentParser(
    description='Coreference Resolution baseline crossvalidation for Marmara Turkish Coreference Corpus')
  parser.add_argument('--outdir', required=True, metavar='OUT', action='store',
    help='Directory for storing output files.')
  parser.add_argument('--doc', required=True, metavar='OUT', action='append',
    help='Treebank file (must be followed by --gold argument).')
  parser.add_argument('--gold', required=True, metavar='OUT', action='append',
    help='Coreference gold XML file.')
  parser.add_argument('--method', default='default', metavar='IN', action='store',
    help='Learning method, see learn() function.')
  parser.add_argument('--predictedmentions', action='store_true',
    help='Predict mentions to train coref model and score on predicted mentions.')
  parser.add_argument('--regbound', metavar='IN', action='append',
    help='Give regression best-link bounds (if omitted, use classification: use all-joins).')
  parser.add_argument('--noclassical', action='store_true',
    help='Do not use classical features (in that case you need to use fasttext for any predictions)')
  parser.add_argument('--fasttext', required=False, metavar='IN', action='store', default='',
    help='Command, model, vectorfile from fasttext for predicting word vectors. Format: executable,modelfile,vectorfile')
  parser.add_argument('--fasttextpca', action='store_true',
    help='Use PCA to reduce fasttext ectors to 15 dimensions.')
  args = parser.parse_args(sys.argv[1:])

  def interpret_twin_list(doclist, goldlist):
    out = []
    for htm, xml in zip(doclist, goldlist):
      m = re.search(r"(?:.*[^0-9])?([0-9a-c]{8,9})\.(?:xml|htm)", htm)
      if not m:
        raise Exception("got malformed input file {}".format(htm))
      doc = m.group(1)
      out.append({'docfile':htm, 'crfile':xml, 'doc':doc})
    if 2*len(out) != (len(doclist) + len(goldlist)):
      raise Exception('need equal number of doc and gold files')
    return out

  # to avoid changing lots of code we also make it global
  OUTDIR = args.outdir
  config = {
    'docs': interpret_twin_list(args.doc, args.gold),
    'predictedmentions': args.predictedmentions,
    'method':args.method,
    'regbounds':args.regbound,
    'regression':False,
  }
  if args.regbound and len(args.regbound) > 0:
    config['regbounds'] = list(map(float, config['regbounds']))
    config['regression'] = True
  if args.fasttext != '':
    knowlpcoref.config['fasttext'] = args.fasttext.split(',')
  if args.fasttextpca:
    knowlpcoref.config['fasttext-use-pca'] = True
  if args.noclassical:
    knowlpcoref.config['classical-features'] = False

if __name__ == '__main__':
  main()
